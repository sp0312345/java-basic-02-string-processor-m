## General requirements
1. Application code must be formatted in a consistent style and comply with the Java Code Convention.
2. If the application contains console menus or Input / Output, then they should be minimal, sufficient and intuitive. Language - English.
3. In the comments for the class, write your name and task number. Add meaningful comments to your code whenever possible.
4. Java Collection Framework (except java.util.Arrays) and regular expressions cannot be used

## Tasks options
Write a program to process strings. The user enters the following data:
1. Number of lines (number)
2. Lines for processing
3. Number of words (number)
4. Words

Implemented methods:
1. In the array of strings, find the shortest string
2. In the array of strings, find the longest string
3. In the array of strings, find strings whose length is less than the average
4. In the array of strings, find strings whose length is greater than the average
5. In the array of words, find a word in which the number of different characters is minimal. If there are several such words, find the first of them
6. In the array of words, find a word consisting only of different symbols. If there are several such words, find the first of them
7. In the array of words, find a word consisting only of numbers. If there are several such words, find the second of them

## Input / Output example
```
Specify amount of lines:
3
Input line 1:
qwerty
Input line 2:
qw
Input line 3:
qwerty qwerty
1) Shortest line is qw (length is 2)
2) Longest line is qwerty qwerty (length is 13)
3) Lines longer than average length:
qwerty qwerty (length is 13)
4) Lines shorter than average length:
qw (length is 2)
qwerty (length is 6)
Specify amount of words:
4
Input word 1:
qqqqqqqqqqwe
Input word 2:
qwer
Input word 3:
123
Input word 4:
4321
5) Word with minimum various characters: qqqqqqqqqqwe
6) Word contains only various characters: qwer
7) Word contains only digits: 4321
```