package com.epam.java_basic.string_processor;

/**
 *  Useful methods for string processing
 */
public class StringProcessor {

    public String findShortestLine(String[] lines) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public String findLongestLine(String[] lines) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public String[] findLinesShorterThanAverageLength(String[] lines) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public String[] findLinesLongerThanAverageLength(String[] lines) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    /**
     * Find word with minimum various characters. Return first word if there are a few of such words.
     * @param words Input array of words
     * @return First word that consist of minimum amount of various characters
     */
    public String findWordWithMinimumVariousCharacters(String[] words) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    /**
     * Find word containing only of various characters. Return first word if there are a few of such words.
     * @param words Input array of words
     * @return First word that containing only of various characters
     */
    public String findWordConsistingOfVariousCharacters(String[] words) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    /**
     * Find word containing only of digits. Return second word if there are a few of such words.
     * @param words Input array of words
     * @return Second word that containing only of digits
     */
    public String findWordConsistingOfDigits(String[] words) {
        throw new UnsupportedOperationException("You need to implement this method");
    }
}
